#!/usr/bin/python3.4

import smbus


class RTC:

    __i2c = smbus.SMBus(1)
    
    __RTC_ADDR = 0x68
    __SEC = 0x00
    __MIN = 0x01
    __HRS = 0x02
    #__WEK = 0x03 # weekday number, currently not implemented
    __DAY = 0x04
    __MON = 0x05
    __YEA = 0x06
    __CTRL = 0x0E
    
    SYSTEM = True
    DISPLAY = False
    
    def __bcdTOdec(self, n):
        lower = n & 0x0F
        return 10*(n >> 4) + lower
        
    def __decTObcd(self, n):
        upper = int(n/10)
        return (upper << 4) + (n % 10)
        

    def __write(self, register, data):
        self.__i2c.write_byte_data(self.__RTC_ADDR, register, data)

    def __read(self, register):
        return self.__i2c.read_byte_data(self.__RTC_ADDR, register)
        
        
    def writeTime(self, year, month, day, hour, minute, second):
        self.__write(self.__YEA, self.__decTObcd(year-2000))
        self.__write(self.__MON, self.__decTObcd(month))
        self.__write(self.__DAY, self.__decTObcd(day))
        self.__write(self.__HRS, self.__decTObcd(hour))
        self.__write(self.__MIN, self.__decTObcd(minute))
        self.__write(self.__SEC, self.__decTObcd(second))
        
    def __addZero(self, value):
        if value < 10:
            return "0" + str(value)
        return str(value)

    def year(self):
        year = self.__bcdTOdec(self.__read(self.__YEA)) + 2000
        return str(year)

    def month(self):
        month = self.__bcdTOdec(self.__read(self.__MON))
        return self.__addZero(month)

    def day(self):
        day = self.__bcdTOdec(self.__read(self.__DAY))
        return self.__addZero(day)

    def hour(self):
        hour = self.__bcdTOdec(self.__read(self.__HRS))
        return self.__addZero(hour)
        
    def minute(self):
        minute = self.__bcdTOdec(self.__read(self.__MIN))
        return self.__addZero(minute)
        
    def second(self):
        second = self.__bcdTOdec(self.__read(self.__SEC))
        return self.__addZero(second)
        
    def formatDate(self, systemFormat):
        
        if systemFormat:
            return "date -s \"" + self.year() + "-" + self.month() + "-" + self.day() + " " + self.hour() + ":" + self.minute() + ":" + self.second() + "\""
            
        return self.day() + "-" + self.month() + "-" + self.year() + " " + self.hour() + ":" + self.minute() + ":" + self.second()
        

    def __init__(self):
        self.__write(self.__CTRL, 0x00) # Init 1Hz square wave

