#!/usr/bin/python3.4

import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

OPEN_DOOR = 11
CLOSE_DOOR = 9
DOOR_LED = 16
HEARTBEAT_LED = 12

OUTS = [OPEN_DOOR, CLOSE_DOOR, DOOR_LED, HEARTBEAT_LED]

GPIO.setup(OUTS, GPIO.OUT)

while True:
    GPIO.output(OUTS, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(OUTS, GPIO.LOW)
    time.sleep(0.5)
