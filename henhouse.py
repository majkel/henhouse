#!/usr/bin/python3.4

import os
import time
import datetime
import threading
import urllib3
import json    
import RPi.GPIO as GPIO
from RTC import *


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


# 12V INPUTS
DOOR_CLOSED = 4
LIGHTS_SWITCH = 17
TWILIGHT_SENSOR = 27
WATER_LEVEL = 22

# 3V3 INPUTS
DOOR_OVERRIDE = 5
LIGHTS_OVERRIDE = 6
WATER_OVERRIDE = 13
OPEN_DOOR = 11
CLOSE_DOOR = 9

# 5V INPUTS
RTC_SQW = 19

INPUTS = [DOOR_CLOSED, 
        LIGHTS_SWITCH,
        TWILIGHT_SENSOR,
        WATER_LEVEL,
        RTC_SQW]

# Pulled-up inputs
INPUTS_PUD = [DOOR_OVERRIDE,
        LIGHTS_OVERRIDE,
        WATER_OVERRIDE,
        OPEN_DOOR,
        CLOSE_DOOR]


# OUTPUTS BCM
LIGHTS = 18
DOOR_A = 23
DOOR_B = 24
WATER = 25
HEARTBEAT_LED = 12
DOOR_LED = 16

#OUTPUTS = [DOOR_A, DOOR_B, HEARTBEAT_LED, DOOR_LED]
OUTPUTS = [HEARTBEAT_LED, DOOR_LED]
RELAY_OUTPUTS = [DOOR_A, DOOR_B, LIGHTS, WATER]


GPIO.setup(INPUTS, GPIO.IN)
GPIO.setup(INPUTS_PUD, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(OUTPUTS, GPIO.OUT, initial=GPIO.LOW)
# Relays are active low
GPIO.setup(RELAY_OUTPUTS, GPIO.OUT, initial=GPIO.HIGH)


def outLO(bcm):
    GPIO.output(bcm, GPIO.LOW)

def outHI(bcm):
    GPIO.output(bcm, GPIO.HIGH)

def read(bcm):
    if GPIO.input(bcm) == 0:
        return True
    return False

def read_output(bcm):
    if GPIO.input(bcm) == 1:
        return True
    return False

def toggle(bcm):
    if read_output(bcm):
        outLO(bcm)
    else:
        outHI(bcm)

def wait_int_falling(bcm):
    GPIO.wait_for_edge(bcm, GPIO.FALLING, bouncetime=350)

def wait_int_rising(bcm):
    GPIO.wait_for_edge(bcm, GPIO.RISING, bouncetime=350)

def setDoorLED():
    if read(DOOR_CLOSED):
        outLO(DOOR_LED)
    else:
        outHI(DOOR_LED)
        
logPath = "/var/www/html/php/log.txt"

def log(text):
    date = datetime.datetime.now()

    text = date.strftime("%a %d-%m-%y %H:%M:%S | ") + text + "\n"

    with open(logPath, "a") as file:
        file.write(text)

def log_verb(text):
    with open(logPath, "a") as file:
        file.write(text + "\n")



class HeartBeatThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):

        while True:

            outHI(HEARTBEAT_LED)
            time.sleep(0.15)
            outLO(HEARTBEAT_LED)
            time.sleep(1.5)



class TimeSetThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def connection_OK(self):
        try:
            http = urllib3.PoolManager()
            request = http.request("GET", "https://www.google.com",
                    timeout=20.0)
            return True
        except:
            return False

        return False


    def run(self):
        
        rtc = RTC()

        time.sleep(60)

        log("Awaria zasilania lub restart systemu")

        if not self.connection_OK():
           os.system(rtc.formatDate(rtc.SYSTEM)) 
           log("Brak połączenia z internetem, ustawiony czas z RTC")
        
        # Calibrate RTC every 5 days (432000 seconds)
 
        rtcDate = ""

        while True:
            if self.connection_OK():
                date = datetime.datetime.now()
                log("Okresowa kalibracja RTC (co 5 dni)")
                
                rtcDate = rtc.formatDate(rtc.DISPLAY)
                log_verb("Czas RTC: " + rtcDate)
                
                rtc.writeTime(date.year, date.month, date.day, date.hour, date.minute, date.second)
                rtcDate = rtc.formatDate(rtc.DISPLAY)
                log_verb("Czas RTC po kalibracji: " + rtcDate)
                
            else:
                log("Nie skalibrowano RTC, brak połączenia z internetem")

            time.sleep(432000)
                    


class LightsSwitchThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        
        while True:
            wait_int_falling(LIGHTS_SWITCH)
            outLO(LIGHTS)
            wait_int_rising(LIGHTS_SWITCH)
            outHI(LIGHTS)



class DoorControlThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):

        global doorAction
        global doorSwitchLock

        if read_output(DOOR_LED):
            outLO(DOOR_LED)
        else:
            outHI(DOOR_LED)
    
        doorSwitchLock = True
        
        if doorAction == 1:
            outLO(DOOR_A)
            
            time.sleep(parameters["open_time"])
            outHI(DOOR_A)

        elif doorAction == 2:
            outLO(DOOR_B)
            
            while not read(DOOR_CLOSED):
                time.sleep(0.1)
            
            outHI(DOOR_B)

        doorSwitchLock = False

        setDoorLED()

        if doorAction > 0:
            doorAction = 0



class DoorSwitchThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):

        global doorSwitchLock
        global doorAction
        global doorTimeLock

        needAction = False
        needToJoin = False

        while True:

            if not doorSwitchLock:
                if read(OPEN_DOOR):
                    doorAction = 1
                    needAction = True

                if read(CLOSE_DOOR):
                    doorAction = 2
                    needAction = True

                if needAction:
                    doorSwitchLock = True
                    doorControlThread = DoorControlThread("Switch door control thread")
                    doorControlThread.start()
                    needAction = False

                if needToJoin and doorTimeLock < 0:
                    doorControlThread.join()
                    needToJoin = False
                
            else:
                if not needToJoin:
                    needToJoin = True

            time.sleep(0.4)


class LightsControlThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):

        global parameters
        
        lightsLock = False
        lights_on_counter = 0
        sec = 60

        time.sleep(30)

        while True:

            if read(TWILIGHT_SENSOR) and not lightsLock and not read(LIGHTS_OVERRIDE):
                outLO(LIGHTS)
                lightsLock = True
                sec = 1
                lights_on_counter = 3600*parameters["lights_on_delay_hrs"] + 60*parameters["lights_on_delay_min"]

            elif not read(TWILIGHT_SENSOR) and lightsLock and lights_on_counter == 0:
                lightsLock = False
                sec = 60

            elif lightsLock:
                if lights_on_counter > 0:
                    lights_on_counter -= 1
                if lights_on_counter == 0 and not read(LIGHTS_SWITCH):
                    outHI(LIGHTS)

            time.sleep(sec)



class WaterPourThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):

        waterLock = False

        time.sleep(30)

        while True:

            if read(WATER_LEVEL) and not read(WATER_OVERRIDE) and not waterLock:
                outLO(WATER)
                time.sleep(parameters["pour_time"])
                outHI(WATER)
                
                if read(WATER_LEVEL):
                    waterLock = True
                    outHI(WATER)

            if waterLock and not read(WATER_LEVEL):
                waterLock = False
                log("Możliwy wyciek wody lub brak pojemnika")


            time.sleep(60)



parameters = {}

def updateParameters():
    global parameters
    try:
        with open("/var/www/html/php/parameters.json", "r") as file:
            parameters =  json.load(file)
    except:
        log("Nie udało się załadować parameters.json")
        parameters = {"open_door_hrs": 5,
            "open_door_min": 0,
            "close_door_hrs": 22,
            "close_door_min": 0,
            "open_time": 5,
            "lights_on_delay_hrs": 5,
            "lights_on_delay_min": 5,
            "pour_time": 10}

        with open("parameters.json", "w") as file:
            json.dump(parameters, file)



def longTimer():
    threading.Timer(10.0, longTimer).start()
    updateParameters()



def doorTimeMatch():
    global parameters
    global clock
    if parameters["open_door_hrs"] == clock.hour and parameters["open_door_min"] == clock.minute:
        return 1
    
    elif parameters["close_door_hrs"] == clock.hour and parameters["close_door_min"] == clock.minute:
        return 2

    return 0

#---------------
# 0 - no action
# 1 - open door
# 2 - close door
doorAction = 0
#---------------
doorTimeLock = -1
doorSwitchLock = False
waterLock = False

heartBeatThread = HeartBeatThread("Heart beat thread")
heartBeatThread.start()

timeSetThread = TimeSetThread("Time set thread")
timeSetThread.start()

lightsSwitchThread = LightsSwitchThread("Lights switch thread")
lightsSwitchThread.start()

doorSwitchThread = DoorSwitchThread("Door switch thread")
doorSwitchThread.start()

lightsControlThread = LightsControlThread("Lights control thread")
lightsControlThread.start()

waterPourThread = WaterPourThread("Water pour thread")
waterPourThread.start()

longTimer()

setDoorLED()

while True:

    clock = datetime.datetime.now()
    if not read(DOOR_OVERRIDE) and not doorSwitchLock:
        doorAction = doorTimeMatch()

    if doorAction > 0 and doorTimeLock < 0:
        doorControlThread = DoorControlThread("Door control thread")
        doorControlThread.start()
        doorTimeLock = clock.minute

    if doorTimeLock >= 0 and doorTimeLock != clock.minute:
        doorControlThread.join()
        doorTimeLock = -1
        doorAction = 0

    if doorSwitchLock:
        toggle(DOOR_LED)


    time.sleep(0.999)

