<?php
	session_start();	

	$file = "php/parameters.json";
	$parameters = json_decode(file_get_contents($file), true);

	function show($var) {
		if (isset($var))
			if ($var)
				echo "style='display: block;'";
			else
				echo "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Kurnik</title>
	<link rel="stylesheet" href="main.css">
	<script type="text/javascript" src="scripts/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="scripts/moment.js"></script>
	<script type="text/javascript" src="scripts/main.js"></script>
</head>
<body>
	<div id="header">
		<h1>kurnik<img src="chicken.png"></h1>
	</div>
	<div id="main">
		<form action="php/save.php" method="post">
		<h3>Ustawienia podstawowe</h3>
		<table>
			<tr>
			<td>Godzina otwarcia drzwi: </td>
			<td>
			<?php echo "<input name='o_d_h' type='number' min='0' max='23' value='".$parameters["open_door_hrs"]."'> h&nbsp;&nbsp;"; ?>
			<?php echo "<input name='o_d_m' type='number' min='0' max='59' value='".$parameters["open_door_min"]."'> m"; ?>
			</td>
			</tr>
			<tr>
			<td>Godzina zamknięcia drzwi: </td>
			<td>
			<?php echo "<input name='c_d_h' type='number' min='0' max='23' value='".$parameters["close_door_hrs"]."'> h&nbsp;&nbsp;"; ?>
			<?php echo "<input name='c_d_m' type='number' min='0' max='59' value='".$parameters["close_door_min"]."'> m"; ?>
			</td>
			</tr>
			<tr>
			<td>Czas świecenia<br>po wyłączeniu zmierzchówki:<br>
			<?php
			$min = 60*$parameters["lights_on_delay_hrs"] + $parameters["lights_on_delay_min"];
			echo "(razem <span class='minutes'>".$min."</span> min.)";
			?>
			</td>
			<td>
			<?php echo "<input id='del-h' name='l_on_h' type='number' min='0' max='10' value='".$parameters["lights_on_delay_hrs"]."'> h&nbsp;&nbsp;"; ?>
			<?php
			echo "<input id='del-m' name='l_on_m' type='number' min='0' max='59' value='".$parameters["lights_on_delay_min"]."'> m"; ?>
			</td>
			</tr>
		</table>
	
		<div id="advanced"><h3>Ustawienia zaawansowane <span class="red">&#x21E9;</span></h3></div>
		
		<div id="adv-settings" <?php show($_SESSION["adv"]); ?>>
			<table>
			<tr>
			<td>Czas otwierania drzwi: </td>
			<td>
			<?php echo "<input name='op_tm' type='number' min='1' max='360' value='".$parameters["open_time"]."'> s <input class='phantom' name='adv_op_tm' type='number' value='".$parameters["pour_time"]."'>"; ?>
			</td>
			</tr>
			<tr>
			<td>Czas nalewania wody: </td>
			<td>
			<?php echo "<input name='wt_tm' type='number' min='1' max='360' value='".$parameters["pour_time"]."'> s <input class='phantom' name='adv_wt_tm' type='number' value='".$parameters["pour_time"]."'>"; ?>
			</td>
			</tr>				
			</table>
		</div>

		<div id="panel"><h3><a href="panel.php">Panel sterowania</a></h3></div>

		<div id="dgn"><h3>Diagnostyka <span class="red">&#x21E9;</span></h3></div>
		<div id="diagnostics" <?php show($_SESSION["dgn"]); ?>>
			<button id="time" class="plain" type="button">Pokaż czas systemu i RTC</button><div id="showtime"></div>
			<br><button class="plain" formaction="php/showlog.php">Pokaż logi</button> 
			<div id="log" <?php show($_SESSION["log"]); ?>>
				<textarea readonly rows="10" cols="60"><?php if(isset($_SESSION["log"]))echo file_get_contents("php/log.txt");?></textarea>
			</div>
		</div>


		<input class="sub" type="submit" value="zapisz zmiany">
		</form>
		
		<?php 
		$pre = "<div class='saved'>";
		$pos = "</div>";
	    if (isset($_GET["status"])) {
		    $status = $_GET["status"];
			   if ($status == "fail-json")
				   echo $pre."Błąd zapisu: kodowanie json!".$pos;
			   elseif ($status == "fail-file")
				   echo $pre."Błąd zapisu do pliku!".$pos;
			   elseif ($status == "success")
				   echo $pre."Zapisano pomyślnie!".$pos;
			   else
				   echo $pre."Błąd zapisu: nieznany błąd".$pos;
		}
	    ?>
	        
	</div>
</body>
</html>

