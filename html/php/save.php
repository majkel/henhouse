<?php 
session_start();
$_SESSION["adv"] = true;

$file = "parameters.json";

$parameters = array("open_door_hrs" => intval($_POST["o_d_h"]),
					"open_door_min" => intval($_POST["o_d_m"]),
					"close_door_hrs" => intval($_POST["c_d_h"]),
					"close_door_min" => intval($_POST["c_d_m"]),
					"lights_on_delay_hrs" => intval($_POST["l_on_h"]),
					"lights_on_delay_min" => intval($_POST["l_on_m"]),
					"open_time" => intval($_POST["op_tm"]),
					"pour_time" => intval($_POST["wt_tm"]));

$location = "Location: ../index.php?status=";

$enc = json_encode($parameters, JSON_FORCE_OBJECT);
$res = file_put_contents($file, $enc);

if (is_null($enc)) $location = $location."fail-json";
elseif (is_null($res)) $location = $location."fail-file";
else $location = $location."success";

header($location);

?>
