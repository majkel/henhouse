$(document).ready(function() {

	$(".saved").delay(3000).fadeOut("slow");
	
	var hrs = 0;
	var min = 0;

	$("#del-h").change(function() {
		hrs = parseInt($(this).val());
		min = parseInt($("#del-m").val());
		min = 60*hrs + min;
		$(".minutes").html(min.toString());
	});

	$("#del-m").change(function() {
		hrs = parseInt($("#del-h").val());
		min = parseInt($(this).val());
		min = 60*hrs + min;
		$(".minutes").html(min.toString());
	});
	
	var toggleAdvanced = true;
	$("#advanced").click(function() {
		if (toggleAdvanced) {
			toggleAdvanced = false;
			$.ajax({url: "../php/show_adv.php"});
			$("#adv-settings").fadeIn("fast");
		}
		else {
			toggleAdvanced = true;
			$.ajax({url: "../php/hide_adv.php"});
			$("#adv-settings").fadeOut("fast");
		}
	});

	var toggleDiagnostics = true;
	$("#dgn").click(function() {
		if (toggleDiagnostics) {
			toggleDiagnostics = false;
			$.ajax({url: "../php/show_dgn.php"});
			$("#diagnostics").fadeIn("fast");
		}
		else {
			toggleDiagnostics = true;
			$.ajax({url: "../php/hide_dgn.php"});
			$("#diagnostics").fadeOut("fast");
		}
	});



	var time = "";
	var rtc = "init";
	$("#time").click(function(){

		$.ajax({
			url: "../php/rtc.php",
			dataType: "text",
			success: function(data) {
				rtc = data;
			}
		}).done(function(){
			$("#showtime").html("<table style='margin-top:5px;'><tr>" 
					+ "<td>System:</td>"
					+ "<td>" + moment().format("DD/MM/YYYY HH:mm:ss") + "</td></tr>"
					+ "<tr><td>RTC:</td>"
					+ "<td>" + rtc + "</td>"
					+ "</tr></table>");
		});
	});
	
});
