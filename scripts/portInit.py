#!/usr/bin/python3.4

import RPi.GPIO as GPIO
import json

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# 12V INPUTS
DOOR_CLOSED = 4
LIGHTS_SWITCH = 17
TWILIGHT_SENSOR = 27
WATER_LEVEL = 22

# 3V3 INPUTS
DOOR_OVERRIDE = 5
LIGHTS_OVERRIDE = 6
WATER_OVERRIDE = 13
OPEN_DOOR = 11
CLOSE_DOOR = 9

INPUTS = [DOOR_CLOSED, 
        LIGHTS_SWITCH,
        TWILIGHT_SENSOR,
        WATER_LEVEL]

# Pulled-up inputs
INPUTS_PUD = [DOOR_OVERRIDE,
        LIGHTS_OVERRIDE,
        WATER_OVERRIDE,
        OPEN_DOOR,
        CLOSE_DOOR]

# 5V INPUTS


# OUTPUTS BCM
LIGHTS = 18
DOOR_A = 23
DOOR_B = 24
WATER = 25
HEARTBEAT_LED = 12
DOOR_LED = 16

#OUTPUTS = [DOOR_A, DOOR_B, HEARTBEAT_LED, DOOR_LED]
OUTPUTS = [HEARTBEAT_LED, DOOR_LED]
RELAY_OUTPUTS = [DOOR_A, DOOR_B, LIGHTS, WATER]


GPIO.setup(INPUTS, GPIO.IN)
GPIO.setup(INPUTS_PUD, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(OUTPUTS, GPIO.OUT)
# Relays are active low
GPIO.setup(RELAY_OUTPUTS, GPIO.OUT)


def outLO(bcm):
    GPIO.output(bcm, GPIO.LOW)

def outHI(bcm):
    GPIO.output(bcm, GPIO.HIGH)

def read(bcm):
    if GPIO.input(bcm) == 0:
        return True
    return False

def read_output(bcm):
    if GPIO.input(bcm) == 1:
        return True
    return False

def toggle(bcm):
    if read_output(bcm):
        outLO(bcm)
    else:
        outHI(bcm)



remoteDir = "/var/www/html/php/remote.json"
remote = {}

def set_remote():
    global remote

    remote["DOOR_CLOSED"] = read(DOOR_CLOSED)
    remote["LIGHTS_SWITCH"] = read(LIGHTS_SWITCH)
    remote["TWILIGHT_SENSOR"] = read(TWILIGHT_SENSOR)
    remote["WATER_LEVEL"] = read(WATER_LEVEL)

    remote["DOOR_OVERRIDE"] = read(DOOR_OVERRIDE)
    remote["LIGHTS_OVERRIDE"] = read(LIGHTS_OVERRIDE)
    remote["WATER_OVERRIDE"] = read(WATER_OVERRIDE)
    remote["OPEN_DOOR"] = read(OPEN_DOOR)
    remote["CLOSE_DOOR"] = read(CLOSE_DOOR)

    remote["LIGHTS"] = read_output(LIGHTS)
    remote["DOOR_A"] = read_output(DOOR_A)
    remote["DOOR_B"] = read_output(DOOR_B)
    remote["WATER"] = read_output(WATER)
    remote["DOOR_LED"] = read_output(DOOR_LED)

    with open(remoteDir, "w") as file:
        json.dump(remote, file)
    

def get_remote():
    global remote
    
    try:
        with open(remoteDir, "r") as file:
            remote = json.load(file)
        print("success")

    except:
        print("get_remote() fail")
