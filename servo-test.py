#!/usr/bin/python3.4

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

DOOR_A = 23
DOOR_B = 24
TICK_LED = 12

GPIO.setup([DOOR_A, DOOR_B, TICK_LED], GPIO.OUT, initial=GPIO.LOW)

def outLO(bcm):
    GPIO.output(bcm, GPIO.LOW)

def outHI(bcm):
    GPIO.output(bcm, GPIO.HIGH)

BCM = DOOR_B

try:
    outHI(BCM)
    outHI(TICK_LED)

    while True:
        time.sleep(0.1)

except KeyboardInterrupt:
    outLO(BCM)
    outLO(TICK_LED)
