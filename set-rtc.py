#!/usr/bin/python3.4

import time
from RTC import *

rtc = RTC()

rtc.writeTime(2017, 8, 31, 20, 5, 30)

print("Time: YEA MON DAY WEK HRS MIN SEC")
print(rtc.year(), rtc.month(), rtc.day(), rtc.hour(), rtc.minute(),
            rtc.second())
